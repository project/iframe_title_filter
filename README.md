# iFrame Title Filter

This text format filter helps ensure that embedded `<iframe>` tags include a `title` attribute,
in order to comply with [WCAG guidelines](https://www.w3.org/TR/WCAG20-TECHS/H64.html).
When an iframe does not have a title attribute, this filter parses the `src` attribute's URL
and adds a title attribute that reads "Embedded content from [url]".A number of Drupal filters
generate iframes (e.g., [media](https://www.drupal.org/project/media), [video_filter](drupal.org/project/video_filter)),
but their compliance with iframe accessibility requirements varies. This filter is meant to
be a universal band-aid to this particular guideline.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/iframe_title_filter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/iframe_title_filter).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Administration » Configuration » Content authoring » Text formats
2. Enable "Add missing titles to iFrames" on any text format for which you expect
   embedded iframes to be present. 
3. Under "Filter processing order," make sure that this filter is set to run after any HTML filtering,
   as well as after any filters that would be generating iFrames (e.g., video_filter).


## Maintainers

- Doug Addison - [daddison](https://www.drupal.org/u/daddison)
- Mark Fullmer - [mark_fullmer](https://www.drupal.org/u/mark_fullmer)
- [bjc2265](https://www.drupal.org/u/bjc2265)
- Paul Grotevant - [gravelpot](https://www.drupal.org/u/gravelpot)
- [Jeff Cardwell](https://www.drupal.org/u/jeff-cardwell)
- Polo Reynaga - [lreynaga](https://www.drupal.org/u/lreynaga)
- [mmarler](https://www.drupal.org/u/mmarler)
- [ricksta](https://www.drupal.org/u/ricksta)
- Jana Tate - [texas_tater](https://www.drupal.org/u/texas_tater)
- Tyler Fahey - [twfahey](https://www.drupal.org/u/twfahey)
- Information Technology Services University of Texas at Austin - [utexas](https://www.drupal.org/u/utexas)
